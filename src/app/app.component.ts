import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my ToDo list';

  todolist = [{
    name: "item 1",
    state: 0;
  }, {
    name: "item 2",
    state: 0;
  }, {
    name: "item 3",
    state: 0;
  }, {
    name: "item 4",
    state: 0;
  }, {
    name: "item 5",
    state: 0;
  }]

  newItemValue = "";


  removeItem(item) {
    var index = this.todolist.indexOf(item);
    this.todolist.splice(index, 1);
  }

  updateItem(item) {
    item.name = prompt("Nouveau nom :", item.name);
  }

  addItem() {
    this.todolist.push({
      name: this.newItemValue,
      state: 0;
    });
    this.newItemValue = "";
  }
}
